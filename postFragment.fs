
precision highp float;

varying float offset;

varying highp vec2 texCoords;
uniform sampler2D screen;

highp vec4  FragColor;

void main(void) {
  vec4 current = texture2D(screen,texCoords+offset);
  vec4 rightone = texture2D(screen,texCoords+2.0*offset);
  vec4 righttwo = texture2D(screen,texCoords+3.0*offset);
  gl_FragColor = vec4(current.r,rightone.g,righttwo.b,1.0);
}
