attribute vec2 vertexPosition;
attribute vec2 aTexCoords;
attribute float aOffset;
varying vec2 texCoords;
varying float offset;
void main(void) {
  gl_Position = vec4(vertexPosition,0.0,1.0);
  texCoords = aTexCoords;
  offset = aOffset;

}
