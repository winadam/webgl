import { Sprite } from "./sprite";
import { Renderer } from "./renderer";

export class ObjMesh extends Sprite {
  constructor(renderer: Renderer, objSource: string) {
    var vertexs: Array<[number, number, number]> = [];
    var faces: Array<[number, number, number]> = [];
    var normals: Array<[number, number, number]> = [];

    var lines = objSource.split("\n");
    for (var line of lines) {
      var args = line.split(" ");
      if (args[0] == "v") {
        vertexs.push([Number(args[1]), Number(args[2]), Number(args[3])]);
      } else if (args[0] == "vn") {
        normals.push([Number(args[1]), Number(args[2]), Number(args[3])]);
      } else if (args[0] == "f") {
        var face = args.slice(1, 4).map(function(e) {
          return Number(e.split("/")[0]) - 1;
        })
        faces.push([face[0], face[1], face[2]]);
      } else {
        //console.warn("unused line", line);
      }
    }
    //console.log(vertexs, faces);
    super(renderer, vertexs, faces);
  }
}
export class ObjLoader {
  static async load(renderer: Renderer, name: string) {
    var sourceRequest = await fetch(name);
    var sourceBuffer = await sourceRequest.arrayBuffer();
    var sourceString = new TextDecoder().decode(sourceBuffer);
    return new ObjMesh(renderer, sourceString);
  }
}
