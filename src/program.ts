function createShader(gl: WebGLRenderingContext, type: number, source: string): WebGLShader {
  var shader = gl.createShader(type);
  if (!shader) { throw "no shader"; }

  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    throw gl.getShaderInfoLog(shader);
  }

  return shader;

}


export class Program {
  gl: WebGLRenderingContext;
  program: WebGLProgram;
  constructor(gl: WebGLRenderingContext, vsSource: string, fsSource: string) {
    this.gl = gl;
    var vertexShader = createShader(gl, gl.VERTEX_SHADER, vsSource);
    var fragShader = createShader(gl, gl.FRAGMENT_SHADER, fsSource);

    var program = gl.createProgram();
    if (!program) { throw "no program" }

    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragShader);
    gl.linkProgram(program);

    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      throw gl.getProgramInfoLog(program);
    }
    this.program = program;
  }
  use() {
    this.gl.useProgram(this.program);
  }
  getUniform(name: string) {
    var uniformLocation = this.gl.getUniformLocation(this.program, name);
    if (!uniformLocation) { throw "no " + name; }
    return uniformLocation;
  }
  getAttrib(name: string) {
    var attribLocation = this.gl.getAttribLocation(this.program, name);
    if (attribLocation == -1) { throw "no " + name; }
    return attribLocation;
  }
}
