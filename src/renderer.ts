import { Sprite } from "./sprite";
import { mat4 } from "gl-matrix";
import { Program } from "./program";




export class Renderer {
  gl: WebGLRenderingContext;
  mainProgram: Program;
  postProcessProgram: Program;
  frameBuffer: WebGLFramebuffer;
  frameBufferTexture: WebGLTexture;

  mMatrixLocation: WebGLUniformLocation;
  vMatrixLocation: WebGLUniformLocation;
  pMatrixLocation: WebGLUniformLocation;

  vMatrix: mat4;
  pMatrix: mat4;

  vertexLocation: number;

  sprites: Array<Sprite> = [];

  vrDisplay?: VRDisplay;
  lastTick: number;

  offsetAmount: number = 1.0 / 500.0;
  constructor(gl: WebGLRenderingContext, mainProgram: Program, postProcessProgram: Program, vrDisplay?: VRDisplay) {
    this.vrDisplay = vrDisplay;
    this.gl = gl;

    this.mainProgram = mainProgram;
    this.postProcessProgram = postProcessProgram;

    this.vertexLocation = this.mainProgram.getAttrib("vertexPosition");
    this.mMatrixLocation = this.mainProgram.getUniform("mMatrix");
    this.vMatrixLocation = this.mainProgram.getUniform("vMatrix");
    this.pMatrixLocation = this.mainProgram.getUniform("pMatrix");


    const fieldOfView = 45 * Math.PI / 180;   // in radians
    var aspect = this.gl.canvas.clientWidth / this.gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;

    this.pMatrix = mat4.create();
    mat4.perspective(this.pMatrix, fieldOfView, aspect, zNear, zFar);

    this.vMatrix = mat4.create();
    mat4.translate(this.vMatrix, this.vMatrix, [0, 0, -6]);

    this.frameBuffer = this.gl.createFramebuffer();
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frameBuffer);

    this.frameBufferTexture = this.gl.createTexture();
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.frameBufferTexture);
    this.gl.texImage2D(this.gl.TEXTURE_2D, 0, this.gl.RGB, this.gl.canvas.width,
      this.gl.canvas.height, 0, this.gl.RGB, this.gl.UNSIGNED_BYTE, null);

    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MIN_FILTER, this.gl.LINEAR);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_MAG_FILTER, this.gl.LINEAR);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_S, this.gl.CLAMP_TO_EDGE);
    this.gl.texParameteri(this.gl.TEXTURE_2D, this.gl.TEXTURE_WRAP_T, this.gl.CLAMP_TO_EDGE);
    this.gl.framebufferTexture2D(this.gl.FRAMEBUFFER, this.gl.COLOR_ATTACHMENT0, this.gl.TEXTURE_2D, this.frameBufferTexture, 0);
    if (this.gl.checkFramebufferStatus(this.gl.FRAMEBUFFER) != this.gl.FRAMEBUFFER_COMPLETE) {
      throw "no framebuffer";
    }

  }
  startVR() {
    this.vrDisplay.requestPresent([{ source: this.gl.canvas }]);
    this.vrDisplay.requestAnimationFrame(this.drawVR.bind(this));

  }
  drawVR(time: number) {
    this.update(time)
    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
    this.clear(0.0, 0.0, 0.0, 1.0);
    var frameData = new VRFrameData();
    this.vrDisplay.getFrameData(frameData);
    var leftEye = this.vrDisplay.getEyeParameters('left');

    this.gl.canvas.width = leftEye.renderWidth;
    this.gl.canvas.height = leftEye.renderWidth;
    this.gl.viewport(0, 0, this.gl.canvas.width / 2, this.gl.canvas.height);


    this.pMatrix.set(frameData.leftProjectionMatrix);
    this.vMatrix.set(frameData.leftViewMatrix);
    this.render();


    this.gl.viewport(this.gl.canvas.width / 2, 0, this.gl.canvas.width / 2, this.gl.canvas.height);
    this.pMatrix.set(frameData.rightProjectionMatrix);
    this.vMatrix.set(frameData.rightViewMatrix);

    this.render();

    this.vrDisplay.submitFrame();

    this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
    this.vrDisplay.requestAnimationFrame(this.drawVR.bind(this));


  }
  clear(r: number, g: number, b: number, a: number) {
    this.gl.clearColor(r, g, b, a);  // Clear to black, fully opaque
    this.gl.clearDepth(1.0);                 // Clear everything
    this.gl.enable(this.gl.DEPTH_TEST);           // Enable depth testing
    this.gl.depthFunc(this.gl.LEQUAL);            // Near things obscure far things
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
  }
  update(time: number) {
    if (!this.lastTick) {
      this.lastTick = time;
      return;
    }
    for (var sprite of this.sprites) {
      sprite.update(time - this.lastTick);
    }
    this.lastTick = time;
  }
  render() {

    this.mainProgram.use();
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frameBuffer);
    this.gl.uniformMatrix4fv(this.pMatrixLocation, false, this.pMatrix)
    this.gl.uniformMatrix4fv(this.vMatrixLocation, false, this.vMatrix)

    for (var sprite of this.sprites) {

      this.gl.uniformMatrix4fv(this.mMatrixLocation, false, sprite.location);

      this.gl.bindBuffer(this.gl.ARRAY_BUFFER, sprite.vertexBuffer);
      this.gl.vertexAttribPointer(this.vertexLocation, 3, this.gl.FLOAT, false, 0, 0);
      this.gl.enableVertexAttribArray(this.vertexLocation);

      this.gl.bindBuffer(this.gl.ELEMENT_ARRAY_BUFFER, sprite.indexBuffer);

      this.gl.drawElements(this.gl.TRIANGLES, sprite.drawCount, this.gl.UNSIGNED_SHORT, 0);



    }
    this.postProcess();
  }
  postProcess() {
    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, null);
    this.gl.clearColor(1.0, 1.0, 1.0, 1.0);
    this.gl.clear(this.gl.COLOR_BUFFER_BIT);

    this.gl.disable(this.gl.DEPTH_TEST);

    this.postProcessProgram.use();
    var screenQuad = [
      -1.0, 1.0, 0.0, 1.0,
      1.0, 1.0, 1.0, 1.0,
      1.0, -1.0, 1.0, 0.0,
      -1.0, -1.0, 0.0, 0.0,
    ];
    var vertexPosition = this.postProcessProgram.getAttrib("vertexPosition");
    var aTexCoords = this.postProcessProgram.getAttrib("aTexCoords");
    var aOffset = this.postProcessProgram.getAttrib("aOffset");
    var screenQuadVertices = this.gl.createBuffer();
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, screenQuadVertices);
    this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(screenQuad), this.gl.STATIC_DRAW);

    this.gl.activeTexture(this.gl.TEXTURE0);
    this.gl.bindTexture(this.gl.TEXTURE_2D, this.frameBufferTexture);
    this.gl.uniform1i(this.postProcessProgram.getUniform("screen"), 0);

    this.gl.enableVertexAttribArray(vertexPosition);
    this.gl.vertexAttribPointer(vertexPosition, 2, this.gl.FLOAT, false, 4 * 4, 0);
    this.gl.enableVertexAttribArray(aTexCoords);
    this.gl.vertexAttribPointer(aTexCoords, 2, this.gl.FLOAT, false, 4 * 4, 2 * 4);

    this.gl.vertexAttrib1f(aOffset, this.offsetAmount);

    this.gl.drawArrays(this.gl.TRIANGLE_FAN, 0, 4);

    this.gl.bindFramebuffer(this.gl.FRAMEBUFFER, this.frameBuffer);

  }
}
