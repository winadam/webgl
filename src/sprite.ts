import { mat4 } from "gl-matrix";
import { Renderer } from "./renderer";

export class Sprite {
  location: mat4;
  vertexBuffer: WebGLBuffer;
  indexBuffer: WebGLBuffer;
  private vertexs: Array<[number, number, number]> = [];
  private indices: Array<[number, number, number]> = [];
  get drawCount() {
    return this.indices.length * 3;
  }
  constructor(renderer: Renderer, points: Array<[number, number, number]>, indices: Array<[number, number, number]>) {
    this.vertexs = points;
    this.indices = indices;
    var vertexBuffer = renderer.gl.createBuffer();
    if (!vertexBuffer) { throw "no vertex buffer" }

    var indexBuffer = renderer.gl.createBuffer();
    if (!indexBuffer) { throw "no index buffer" }

    renderer.gl.bindBuffer(renderer.gl.ARRAY_BUFFER, vertexBuffer);

    var flattenPoints: number[] = [];
    for (var point of points) {
      flattenPoints.push(point[0], point[1], point[2]);
    }
    renderer.gl.bufferData(renderer.gl.ARRAY_BUFFER, new Float32Array(flattenPoints), renderer.gl.STATIC_DRAW);

    var flattenIndices: number[] = [];
    for (var index of indices) {
      flattenIndices.push(index[0], index[1], index[2]);
    }

    renderer.gl.bindBuffer(renderer.gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
    renderer.gl.bufferData(renderer.gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(flattenIndices), renderer.gl.STATIC_DRAW);

    this.vertexBuffer = vertexBuffer;
    this.indexBuffer = indexBuffer;

    this.location = mat4.create();
  }
  update(deltaTime: number) {
    mat4.rotateY(this.location, this.location, 1 * (deltaTime / 1000));
    mat4.rotateZ(this.location, this.location, 1 * (deltaTime / 1000));
  }
}
