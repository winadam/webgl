import { Renderer } from "./renderer";
import { ObjLoader } from "./objloader";
import { Program } from "./program";

async function download(file: string) {
  var decoder = new TextDecoder("utf-8");
  var fileRequest = await fetch(file);
  var fileBuffer = await fileRequest.arrayBuffer();
  return decoder.decode(fileBuffer);
}

async function load(canvas: HTMLCanvasElement) {


  var vertexShader = await download("vertex.vs");
  var fragShader = await download("fragment.fs");

  var postVertexShader = await download("postVertex.vs");
  var postFragShader = await download("postFragment.fs");

  var displays: any[] = []
  if (navigator.getVRDisplays) {
    displays = await navigator.getVRDisplays();
  }

  var gl = canvas.getContext("webgl");
  if (!gl) { throw "no gl"; }

  var mainProgram = new Program(gl, vertexShader, fragShader);
  var postProcessProgram = new Program(gl, postVertexShader, postFragShader);

  var renderer = new Renderer(gl, mainProgram, postProcessProgram, displays[0])

  if (displays.length != 0) {
    var enableVR = document.createElement("button");
    enableVR.innerText = "Enable VR";
    enableVR.onclick = renderer.startVR.bind(renderer);
    document.body.appendChild(enableVR);
  }

  var slider = document.createElement("input");
  slider.type = "range";
  slider.min = "50";
  slider.max = "500";
  slider.value = String(1 / renderer.offsetAmount);
  slider.oninput = (e) => {
    console.log(slider.value);
    renderer.offsetAmount = 1.0 / Number(slider.value);
  }
  document.body.appendChild(slider);

  return renderer;

}

var canvas = document.createElement("canvas");
document.body.appendChild(canvas);
canvas.width = canvas.height = 800;
load(canvas).then(function(renderer) {
  console.log(renderer);
  // var canvas = renderer.gl.canvas;
  // canvas.width = 640;
  // canvas.height = 480;

  ObjLoader.load(renderer, "LibertStatue.obj").then(function(e) {
    renderer.sprites.push(e);
    requestAnimationFrame(gameLoop.bind(null, renderer));
  });
  //  gameLoop(renderer);
}).catch(console.warn);
function gameLoop(renderer: Renderer, time: number) {
  //renderer.update(time);
  renderer.clear(0.0, 0.0, 0.0, 1.0);
  renderer.update(time);
  renderer.render();
  requestAnimationFrame(gameLoop.bind(null, renderer));
}
