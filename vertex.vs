attribute vec4 vertexPosition;
uniform mat4 mMatrix;
uniform mat4 vMatrix;
uniform mat4 pMatrix;

void main(void) {
  gl_Position = pMatrix * vMatrix * mMatrix * vertexPosition;

}
